class Instrumento:
    def __init__(self,precio):
        self.vprecio=precio
    def tocar(self):
        print('estamos tocando musica')
    def romper(self):
        print(f'Son {str(self.vprecio)} Soles')
class Bateria(Instrumento):
#    def __init__()
    pass
class Flauta(Instrumento):
    def __init__(self,vprecio,marca):
        self.vmarca=marca
        super().__init__(vprecio)
#sobreeescritura de metodo
    def tocar(self):
        print("suena la trompeta")
    pass

tambor=Instrumento(20)
tambor.tocar()
tambor.romper()

miflauta=Flauta(49,'yamaha')
miflauta.romper()
miflauta.tocar()
print(miflauta.vprecio)
