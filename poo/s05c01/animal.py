class Gato():
    type_animal='mamifero'
    __vidas=7
    def __init__(miself, nombre,edad,raza,alimentos='pan', **params):
        miself.vnombre=nombre
        miself.vedad=edad
        miself.vraza=raza
        miself.valimentos=alimentos
    def sumar(miself):
        return miself.valor1 + miself.valor2
    def validar_edad(miself):
        if miself.vedad > 7:
            return print('el gato es viejo')
        else:
            return print('el gatos es un bebe')
    def buscar_alimento(miself,palimento):
        return print(palimento in miself.valimentos)
    def getVidas(self):
        return self.__vidas
    def setVidas(self,parametro):
        self.__vidas=parametro
    def metodo_multi_parametros(miself, **params):
        print(params['a'])
        print(params['b'])
def otroMetodo(parametro='por defecto',otro_parametro='es un valor'):
    pass
        
gato1=Gato('michifu', 8, 'angora',['leche','pan','galletas'])
#valor=gato1.validar_edad()
#print(valor)
gato1.validar_edad()
gato1.buscar_alimento('leche')
gato1.buscar_alimento('pan')
# si hay un return puedes printear def func(): return valor ... print(func())
print(gato1.getVidas())
gato1.setVidas(8)
#pero si hay un print solo llamala def func(): print(valor) ... func()
print(gato1.getVidas())
hash_data={"a":1,"b":"hi iam number one"}
gato1.metodo_multi_parametros(**hash_data)
