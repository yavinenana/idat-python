>>> my_string = "{}, is a {} {} science portal for {}"
>>> print(my_string)
{}, is a {} {} science portal for {}
>>> 
>>> print (my_string.format("v1","v2","v3"))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: tuple index out of range
>>> print (my_string.format("v1","v2","v3","v4"))   
v1, is a v2 v3 science portal for v4



dict={'nombre':'carlos','edad':27,'sexo':'m','nombre':'jordy'}
for key in dict:
    print(key),' : ',dict[key] 

nombre
(None, ' : ', 'jordy')
edad
(None, ' : ', 27)
sexo
(None, ' : ', 'm')

>>> diccionario = {'nombre' : 'Carlos', 'edad' : 22, 'cursos': ['Python','Django','JavaScript'] }
>>> print(diccionario['nombre']
... 
... 
... 
... 
... )
Carlos
>>> print(diccionario['cursos']
... )
['Python', 'Django', 'JavaScript']
>>> print(diccionario['cursos'][0])
Python

>>> for key in diccionario:
... 	print(key," : ",diccionario[key]
... 
... )
... 
nombre  :  Carlos
edad  :  22
cursos  :  ['Python', 'Django', 'JavaScript']

