# -*- coding: utf-8 -*-
file='/home/rodrigo/Documents/idat/poo/s05c02-handle-files/texto.txt'
texto='123456'
try:
    file = open(file, 'r+')
    agregar = 'linea: '+str(texto)
    file.write(agregar)
    print(file.read())
    file.seek(0)
except Exception as e:
    print(f'Error: {str(e)}')
else:
    file.close()

# el r+ sobreescribe las lineas existentes
#file.txt
# 1
# 2
# 3
# 4
#-- el r+  en file.write('123456') escribira :
# 123456
