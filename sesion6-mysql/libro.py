from conn import conexion as con
#from conn.conexion import Conexion
class Libro():
    def __init__(self,titulo,autor,anio):
        self.vtitulo=titulo
        self.vautor=autor
        self.vanio=anio
#        self.create_table()
    def create_table(self):
        try:
            print('                           -- create_table --')
            conn=con.Conexion()
            query = """
            CREATE TABLE IF NOT EXISTS libros(
                id INT NOT NULL AUTO_INCREMENT,
                titulo VARCHAR(150) NULL,
                autor VARCHAR(80) NULL,
                anio INT NULL,
                PRIMARY KEY(id)
                );
            """
            conn.ejecutar_sentencia(query)
            conn.commit()
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
    def agregarLibro(self,libro):
        print('                           -- agregarLibro --')
        try:
            conn=con.Conexion()
# es lo mismo que use self.atributo o objeto.atributo
# self.vtitulo = libro.vtitulo
            query="""
            Insert Into libros(titulo,autor,anio)
            values ('{}','{}','{}')""".format(self.vtitulo,self.vautor,self.vanio)
            conn.ejecutar_sentencia(query)
            conn.commit()
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
    def actualizarLibro(self,id_libro,libro):
        print('                           -- actualizarLibro --')
        try:
            conn=con.Conexion()
            query="""update libros set titulo='{}',autor='{}',anio={} where id='{}' 
            """.format(libro.vtitulo,libro.vautor,libro.vanio,id_libro)
            conn.ejecutar_sentencia(query)
            conn.commit()
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
    def listarLibro(self):
        listadoLibros=[]
        try:
            print('                           -- listarLibro --')
            conn=con.Conexion()
            query="""
            select * from libros ;
            """
            cursor=conn.ejecutar_sentencia(query)
            print("elcursor: ",cursor)
            filas=cursor.fetchall()
            for fila in filas:
                print(fila)
#                print("#########")
                print (fila[0])
#                libro = libro = Libro(fila[1],fila[2],fila[3])
#                listadoLibros.append(libro)
#                print (listadoLibros)
            print("va a retornar::::::::::::::")
#            return listadoLibros
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
    def obtenerLibro(self,id_libro):
        print('                           -- obtenerLibro --')
        try:
#            libro=[]
#            Libro=[]
            conn=con.Conexion()
            query="""select * from libros where id='{}'""".format(id_libro)
            cursor=conn.ejecutar_sentencia(query)
            fila=cursor.fetchone()
#            fila=cursor.fetchall()
            # donde:
            # fila[0]=id    |
            # fila[1]=titulo|
            # fila[2]=autor |
            # fila[3]=anio  |
            print(fila)
#            if libro!=None:
            libro=Libro(fila[1],fila[2],fila[3])
            print (libro)
            print('\n')
#            for f in fila:
#                print(f[1],f[2],f[3])
#            print("return: fila: ")
            return libro
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
    def eliminarLibro(self,id_libro,libro):
        print('                           -- eliminarLibro --')
        try:
            conn=con.Conexion()
            query="""delete from libros where id='{}' """.format(id_libro)
#            query = f'''
#                delete from libros where id = {id_libro}
#            '''
            conn.ejecutar_sentencia(query)
            conn.commit() 
            return True
        except Exception as e:
            raise
            print(e)
        finally:
            conn.cerrar_conexion()
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
