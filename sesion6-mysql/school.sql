alter table libros modify titulo varchar(30);

create table t1(id int primary key,name varchar(50));
create table t2(id int,name varchar(50),primary key(id));
alter table t3 add primary key(id);
create table t7(id int not null, id_cliente int, name varchar(20) , primary key(id),foreign key(id_cliente) references t6(id));
#char vs varchar : char(10) completa los 10- varchar(10) llena todos +1

create database school;
use school;
create table actors(actor_id int not null auto_increment, actor_name varchar(255) not null, primary key(actor_id));

create table customer(
	id int not null auto_increment,
	FirstName char(20) ,
	LastName char(20),
	City varchar(20),
	Country char(29),
	Phone int(9),
	primary key(id)
);
create table product(
        id_product int not null auto_increment,
        ProductName char(20) ,
        Supplierld char(20),
        UnitPrice varchar(20),
        Package char(29),
        IsDiscontinued int(9),
        primary key(id_product)
);
create table client()

insert into customer(FirstName,LastName,City,Country,Phone) 
values ('jordy freddy','Peña Barriga','lima','peru',989805490),
	('yuleidi','hernandez laguna','san carlos','venezuela',987767656),
	('joel daniel','peña julca','lima','peru',988234234)

insert into customer(FirstName,LastName,City,Country,Phone)
values ('roxy','veronica andreina','carcas','venezuela',987264546),
	('dayan','jose garmendia','san carlos','venezuela',969253333),
	('maria jose','reyes','san carlos','caracas',978222228)

update customer 
set 	FirstName='karen',
	LastName='leon valenzuela',
	City='lima',
	Country='Peru'
where id=7;

create table profe(
	id_profe int not null auto_increment,
	name varchar(8),
	primary key(id_profe)
)
create table curso(
	id_curso int not null auto_increment,
	id_profe_curso int,
	name varchar(8),
	primary key(id_curso),	
	foreign key(id_profe_curso) references profe(id_profe)
)

-----------------------------------------------------------------------
create table client(
        id_client int not null auto_increment,
        name varchar(20),
        last_name varchar(20),
        address varchar(20),
        primary key(id_client)
);
create table product(
        id_product int not null auto_increment,
        name varchar(20),
        price float,
        stock int(8),
        primary key (id_product)
);
create table invoice(
        id_invoice int not null auto_increment,
        id_client int(8),
        date datetime,
	primary key(id_invoice),
	foreign key(id_client) references client(id_client)
);
create table detail_invoice(
        id_detail_invoice int not null auto_increment,
	id_invoice int,
        id_product int,
        quantity int,
        price float,
        primary key (id_detail_invoice),
	foreign key(id_invoice) references invoice(id_invoice),
	foreign key(id_product) references product(id_product)
);
-----------------------------------------------------------------
create database library;
use library;
create table libros(
        id int not null auto_increment,
        titulo varchar(8),
        autor varchar(8),
        anio int,
        primary key(id)
)
