import pymysql;

class Conexion():
    def __init__(self,server='localhost',user='root',pwd='S0p0rt33',database='library'):
        self.db = pymysql.connect(server,user,pwd,database)
        self.cursor = self.db.cursor()
        print('Conexion sucessfully :)')
    def ejecutar_sentencia(self,sql):
        self.cursor.execute(sql)
# cursor.fetchall -- obtenemos el resultado de la ejecucion de la consulta
#        filas=self.cursor.fetchall()
# cursor.fetchmany(size)
# cursor.fetchone()
# luego recorremos el resultado
#        for fila in filas:
#            print(fila,'\n')
#            libro=libro=Libro()
#        print (self.cursor)
        return self.cursor
#        records=cursor.fetchall()
#        print("Total rows are:  ", len(records))
#        print("Printing each row")
#        for row in records:
#            print("Id: ", row[0])
#            print("Name: ", row[1])
#            print("Email: ", row[2])
#            print("Salary: ", row[3])
#            print("\n")
#        cursor.close()
# cursor.fetchmany(size)
# cursor.fetchone()
# luego recorremos el resultado
    def commit(self):
        self.db.commit()
        return
    def rollback(self):
        self.db.rollback()
        return
    def cerrar_conexion(self):
        print ("connection closed .")
        self.db.close()
