-- CREANDO BD library
create database if not exists library
GO
-- USANDO BD library
use library
GO
-- CREANDO TABLA libros
create table libros(
        id int not null auto_increment,
        titulo varchar(40),
        autor varchar(30),
        anio int,
        primary key(id)
)
GO
-- INSERTANDO 4 libros
INSERT INTO libros(titulo,autor,anio) VALUES('el corazon delator','Edgar Allan Poe',1843)
INSERT INTO libros(titulo,autor,anio) VALUES('los inocentes','Oswaldo Reynoso',1961)
INSERT INTO libros(titulo,autor,anio) VALUES('En Octubre no hay milagros','Oswaldo Reynoso',1965)
INSERT INTO libros(titulo,autor,anio) VALUES('Javier Heraud','El rio',1960)
GO
-- VISUALIZANDO TODA LA TABLA DISTRITOS
SELECT * FROM libros
GO
