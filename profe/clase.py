# -*- coding: utf-8 -*-


class Televisor:
    def __init__(self, marca, color, precio):
        self.marca = marca
        self.color = color
        self.precio = precio


class Archivo:
    def __init__(self, nombre_archivo):
        self.nombre_archivo = nombre_archivo

    def mostrar_archivo(self):
        try:
            file = open(self.nombre_archivo, 'r')
            for linea in file.readlines():
                print(linea)
        except Exception as e:
            print(f'Error: {str(e)}')
        else:
            file.close()

    def agregar_televisor(self, televisor):
        try:
            file = open(self.nombre_archivo, 'a')
            agregar = "{}, {}, {} \n".format(televisor.marca, televisor.color, televisor.precio)
            file.write(agregar)
        except Exception as e:
            print(f'Error: {str(e)}')
        else:
            file.close()
            print(file)


televisor = Televisor('Samsung', 'Azul', 's/ 2500')
archivo = Archivo('televisores.txt')
archivo.agregar_televisor(televisor)
archivo.mostrar_archivo()